package uwm

import (
	"errors"

	"github.com/BurntSushi/xgb/xproto"
)

// Option represents a Window modifier.
// Nil Option values will be ignored.
type Option func(*Window) error

// Display sets the X11 display name/socket;
// this will not succeed in Window.Create calls.
//
func Display(display string) Option {
	return func(w *Window) error {
		if !w.IsTopLevel() {
			msg := "uwm: Display cannot be passed to a managed sub-window"
			return errors.New(msg)
		}

		w.top.display = display

		return nil
	}
}

// Parent sets the parent window ID;
// this will not succeed in Window.Create calls.
//
func Parent(id WindowID) Option {
	return func(w *Window) error {
		if !w.IsTopLevel() {
			msg := "uwm: Parent cannot be passed to a managed sub-window"
			return errors.New(msg)
		}

		w.top.parentID = xproto.Window(id)

		return nil
	}
}

// Name sets the title of the top-level window.
func Name(s string) Option {
	return func(w *Window) error {
		if !w.IsTopLevel() {
			msg := "uwm: Name cannot be passed to a managed sub-window"
			return errors.New(msg)
		}

		w.top.name = s

		return nil
	}
}

// BGColor sets the background color for the window.
func BGColor(color Color) Option {
	return BGColor32(fromColor(color))
}

// BGColor32 sets the background color for the window.
func BGColor32(color Color32) Option {
	return func(w *Window) error {
		w.flags |= xproto.CwBackPixel
		w.bgColor = fromColor(color)

		return nil
	}
}

// Undecorated marks the window as being undecorated
// and uncontrolled by the window-manager.
// This may only be passed to the top-level window.
//
func Undecorated() Option {
	return func(w *Window) error {
		if !w.IsTopLevel() {
			msg := "uwm: Undecorated cannot be passed to a managed sub-window"
			return errors.New(msg)
		}

		w.flags |= xproto.CwOverrideRedirect

		return nil
	}
}

// Floating marks the window as being floating rather than tiled,
// for typical tiling window managers.
//
func Floating() Option {
	return func(w *Window) error {
		if !w.IsTopLevel() {
			msg := "uwm: Floating cannot be passed to a managed sub-window"
			return errors.New(msg)
		}

		w.top.floating = true

		return nil
	}
}
