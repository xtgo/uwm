// Package uwm implements a micro/pseudo X11 window-manager:
// not at all an actual window manager,
// but just one that can expose a window with sub-windows,
// and toggle between them.
//
package uwm

import (
	"errors"
	"fmt"
	"sync"

	"github.com/BurntSushi/xgb/xproto"
	"github.com/BurntSushi/xgbutil"
	"github.com/BurntSushi/xgbutil/ewmh"
	"github.com/BurntSushi/xgbutil/xwindow"
)

var (
	errClosed   = errors.New("uwm: connection closed or call to nil window")
	errNotChild = errors.New("uwm: given window is not a child of the receiver")
	errBadGeom  = errors.New("uwm: invalid geometry")
)

// Create returns a new window with the given geometry and options.
// Sub-windows should be created by calling the Window.Create method.
// A Close of any parent will implicitly close all children.
//
// See Window for documentation on concurrent-safety.
//
func Create(g Geom, opts ...Option) (*Window, error) {
	if !g.IsValid() {
		return nil, fmt.Errorf("uwm: invalid geom %s", g)
	}

	w := &Window{geom: g}
	w.top = &top{window: w}

	err := applyOpts(w, opts...)
	if err != nil {
		return nil, err
	}

	conn, err := xgbutil.NewConnDisplay(w.top.display)
	if err != nil {
		return nil, err
	}

	if w.top.parentID == 0 {
		w.top.parentID = conn.RootWin()
	}

	parent := xwindow.New(conn, w.top.parentID)

	err = create(w, parent)
	if err != nil {
		return nil, err
	}

	return w, nil
}

// Window represents a displayed region that may have sub-windows.
// Sub-windows have positions relative to their parent,
// rather than the screen itself.
//
// Concurrent accesses between a parent and children are generally not safe:
// this includes concurrent Close of parent and child
// (closing parent will close all children),
// as well as UpdateGeom on one of the children
// involved in a parent's SubSwapGeom.
// However, it is safe to concurrently Create children of the same parent,
// and to use Show/Hide on a child alongside SubShowOnly on a parent.
//
type Window struct {
	top *top
	x11 *xwindow.Window

	parent   *Window
	children sync.Map

	geom    Geom
	flags   uint32 // corresponds to xproto.Cw* constants
	bgColor Color32
}

type top struct {
	window   *Window
	display  string
	name     string
	parentID xproto.Window
	floating bool
}

// IsTopLevel returns true if the given window
// was produced by the global Create function.
//
func (w *Window) IsTopLevel() bool {
	return w != nil && w == w.top.window
}

// ID returns the X11 window ID associated with the window.
func (w *Window) ID() WindowID {
	if w.IsClosed() {
		return 0
	}

	return WindowID(w.x11.Id)
}

// Geom returns the geometry associated with the window.
func (w *Window) Geom() Geom {
	if w.IsClosed() {
		return Geom{}
	}

	return w.geom
}

// UpdateGeom repositions and/or resizes the receiver.
func (w *Window) UpdateGeom(g Geom) error {
	if !g.IsValid() {
		return errBadGeom
	}

	if w.IsClosed() {
		return errClosed
	}

	var err error

	if !w.IsTopLevel() || w.flags&xproto.CwOverrideRedirect != 0 {
		w.x11.MoveResize(int(g.X), int(g.Y), int(g.W), int(g.H))
	} else {
		// a top-level, non-overridden window should be moved via WM hints
		err = w.x11.WMMoveResize(int(g.X), int(g.Y), int(g.W), int(g.H))
	}

	if err != nil {
		return err
	}

	w.geom = g

	return err
}

// SubShowOnly hides all children but the given sub-window.
// If the given window is not one of the receiver's direct children,
// no action is taken.
//
func (w *Window) SubShowOnly(sub *Window) error {
	if w.IsClosed() {
		// w cannot have any sub-windows, thus we treat this as a failure.
		return errClosed
	}

	_, ok := w.children.Load(sub)
	if !ok {
		return errNotChild
	}

	var subErr error

	w.children.Range(func(key, _ interface{}) bool {
		child := key.(*Window)
		if child == nil || sub == child {
			return true
		}

		err := child.Hide()
		if subErr == nil {
			subErr = err
		}

		return true
	})

	// map the new window last to avoid redundant redraws
	err := sub.Show()
	if err != nil {
		return err
	}

	return subErr
}

// SubSwapGeom exchanges the geometries of the two given sub-windows.
// If the given windows are not the receiver's direct children,
// no action is taken.
//
func (w *Window) SubSwapGeom(sub1, sub2 *Window) error {
	if w.IsClosed() {
		// w cannot have any sub-windows, thus we treat this as a failure.
		return errClosed
	}

	_, ok := w.children.Load(sub1)
	if !ok {
		return errNotChild
	}

	_, ok = w.children.Load(sub2)
	if !ok {
		return errNotChild
	}

	if sub1 == sub2 {
		// a self-swap can be treated as a no-op.
		return nil
	}

	g1 := sub1.Geom()
	g2 := sub2.Geom()

	err := sub1.UpdateGeom(g2)
	if err != nil {
		return err
	}

	return sub2.UpdateGeom(g1)
}

// Show marks the receiver as visible,
// though the window will only be displayed
// if all ancestor windows are marked as visible.
//
func (w *Window) Show() error {
	if !w.IsClosed() {
		w.x11.Map()
	}

	return nil
}

// Hide marks the receiver as invisible.
func (w *Window) Hide() error {
	if !w.IsClosed() {
		w.x11.Unmap()
	}

	return nil
}

// Create initializes a sub-window of the receiver.
// The geometry is interpreted relative to the top-left corner
// of the receiver.
//
func (w *Window) Create(g Geom, opts ...Option) (*Window, error) {
	if w.IsClosed() {
		return nil, errClosed
	}

	dst := &Window{top: w.top, parent: w, geom: g}

	err := applyOpts(dst, opts...)
	if err != nil {
		return nil, err
	}

	err = create(dst, w.x11)
	if err != nil {
		return nil, err
	}

	w.children.Store(dst, struct{}{})

	return dst, nil
}

// IsClosed returns true if the window has been closed,
// and is therefore no longer renderable.
//
func (w *Window) IsClosed() bool {
	return w == nil || w.x11 == nil
}

// Close destroys the window, as well as all sub-windows of this window.
// All subsequent calls to other methods will return an error or zero value.
func (w *Window) Close() error {
	if w.IsClosed() {
		return nil
	}

	x11 := w.x11

	w.x11 = nil

	if w.IsTopLevel() {
		defer x11.X.Conn().Close()
	}

	defer x11.Destroy()

	if w.parent != nil {
		defer w.parent.remove(w)
		w.parent = nil
	}

	w.children.Range(func(key, _ interface{}) bool {
		key.(*Window).Close()
		return true
	})

	return nil
}

func (w *Window) remove(sub *Window) {
	if w != nil && sub != nil {
		w.children.Delete(sub)
	}
}

func applyOpts(dst *Window, opts ...Option) error {
	for _, opt := range opts {
		if opt == nil {
			continue
		}

		err := opt(dst)
		if err != nil {
			return err
		}
	}

	return nil
}

func create(dst *Window, parent *xwindow.Window) error {
	x11, err := xwindow.Generate(parent.X)
	if err != nil {
		return err
	}

	dst.x11 = x11

	var flags int
	var values []uint32

	if dst.flags&xproto.CwBackPixel != 0 {
		flags |= xproto.CwBackPixel
		values = append(values, uint32(dst.bgColor))
	}

	if dst.flags&xproto.CwOverrideRedirect != 0 {
		flags |= xproto.CwOverrideRedirect
		values = append(values, 1)
	}

	g := dst.geom

	err = x11.CreateChecked(parent.Id,
		int(g.X), int(g.Y), int(g.W), int(g.H),
		flags, values...)

	if err != nil {
		x11.Destroy()
		return err
	}

	if dst.IsTopLevel() && dst.top.name != "" {
		err = ewmh.WmNameSet(x11.X, x11.Id, dst.top.name)
		if err != nil {
			return err
		}
	}

	if dst.IsTopLevel() && dst.top.floating {
		err = ewmh.WmWindowTypeSet(x11.X, x11.Id, []string{"_NET_WM_WINDOW_TYPE_UTILITY"})
		if err != nil {
			return err
		}
	}

	x11.WMGracefulClose((*xwindow.Window).Destroy)

	x11.Map()

	return nil
}
