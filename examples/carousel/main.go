package main

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/xtgo/uwm"
)

func main() {
	err := run()
	if err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	g := uwm.Geom{W: 1260, H: 180}

	w, err := uwm.Create(g, uwm.Name("carousel"), uwm.Floating())
	if err != nil {
		return err
	}

	defer w.Close()

	colors := []uwm.Color32{0xff0000, 0x00ff00, 0x0000ff}

	geoms := []uwm.Geom{
		{X: 0, Y: 0, W: 420, H: 180},
		{X: 420, Y: 0, W: 420, H: 180},
		{X: 840, Y: 0, W: 420, H: 180},
	}

	n := len(geoms)

	wins := make([]*uwm.Window, n)

	for i, geom := range geoms {
		color := colors[i]

		c, err := w.Create(geom, uwm.BGColor32(color))
		if err != nil {
			return err
		}

		wins[i] = c
		fmt.Println("window id:", c.ID())
	}

	tick := time.NewTicker(3 * time.Second)
	i := 0

	for {
		<-tick.C
		i = (i + 1) % n

		for j, win := range wins {
			k := (i + j) % n
			geom := geoms[k]
			err := win.UpdateGeom(geom)
			if err != nil {
				return err
			}
		}
	}
}
