package main

import (
	"log"
	"time"

	"gitlab.com/xtgo/uwm"
)

func main() {
	err := run()
	if err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	g := uwm.Geom{W: 600, H: 600}

	w, err := uwm.Create(g, uwm.Name("flip"), uwm.Floating())
	if err != nil {
		return err
	}

	defer w.Close()

	a, err := w.Create(g)
	if err != nil {
		return err
	}

	a.Create(uwm.Geom{X: 0, Y: 0, W: 300, H: 300}, uwm.BGColor32(0xff0000))
	a.Create(uwm.Geom{X: 300, Y: 0, W: 300, H: 300}, uwm.BGColor32(0x00ff00))
	a.Create(uwm.Geom{X: 300, Y: 300, W: 300, H: 300}, uwm.BGColor32(0x0000ff))
	a.Create(uwm.Geom{X: 0, Y: 300, W: 300, H: 300}, uwm.BGColor32(0xff00ff))

	b, err := w.Create(g)
	if err != nil {
		return err
	}

	b.Create(uwm.Geom{X: 0, Y: 0, W: 300, H: 300}, uwm.BGColor32(0xffff00))
	b.Create(uwm.Geom{X: 300, Y: 0, W: 300, H: 300}, uwm.BGColor32(0x00ffff))
	b.Create(uwm.Geom{X: 300, Y: 300, W: 300, H: 300}, uwm.BGColor32(0xffffff))
	b.Create(uwm.Geom{X: 0, Y: 300, W: 300, H: 300}, uwm.BGColor32(0x000000))

	windows := []*uwm.Window{a, b}

	i := 0
	tick := time.NewTicker(3 * time.Second)

	for {
		w.SubShowOnly(windows[i])
		<-tick.C
		i = (i + 1) % len(windows)
	}
}
