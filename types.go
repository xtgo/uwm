package uwm

import (
	"fmt"
)

// WindowID represents an X11 window identifier
type WindowID uint32

// Geom represents a display region.
type Geom struct {
	_ struct{} // prevent importers from using a non-keyed struct literal
	W uint16   `json:"w,omitempty"`
	H uint16   `json:"h,omitempty"`
	X uint16   `json:"x,omitempty"`
	Y uint16   `json:"y,omitempty"`
}

// IsZero returns true if all fields are zero.
func (g Geom) IsZero() bool {
	return g == Geom{}
}

// String returns a conventional X11 geometry string representation.
func (g Geom) String() string {
	return fmt.Sprintf("%dx%d%+d%+d", g.W, g.H, g.X, g.Y)
}

// IsValid returns true if the receiver represents a non-empty region.
func (g Geom) IsValid() bool {
	return g.W > 0 && g.H > 0
}

// NoXY returns a copy of the receiver
// in which the X and Y offset are reset to zero.
//
func (g Geom) NoXY() Geom {
	return Geom{W: g.W, H: g.H}
}

// Overlaps returns true if any part of g strictly covers,
// or is covered by, any part of h.
// Geometries that are merely adjacent are not considered overlapping.
//
func (g Geom) Overlaps(h Geom) bool {
	switch {
	case g.X+g.W <= h.X, h.X+h.W <= g.X:
		return false
	case g.Y+g.H <= h.Y, h.Y+h.H <= g.Y:
		return false
	}

	return true
}

// Union produces a new Geom that fully contains g and h.
func (g Geom) Union(h Geom) Geom {
	x1 := minU16(g.X, h.X)
	y1 := minU16(g.Y, h.Y)
	x2 := maxU16(g.X+g.W, h.X+h.W)
	y2 := maxU16(g.Y+g.H, h.Y+h.H)

	return Geom{X: x1, Y: y1, W: x2 - x1, H: y2 - y1}
}

// Partition returns a new Geom representing the subregion of the receiver
// corresponding to the given row and col (zero-indexed)
// when uniformly divided into a grid of rows and cols.
//
// If used to position subwindows, call NoXY on the receiver first,
// since sub-window positions are relative to their parent.
//
// Partition will panic if rows or cols is less than 1,
// or row or col is semantically out-of-bounds.
//
func (g Geom) Partition(rows, cols, row, col int) Geom {
	if rows <= 0 || cols <= 0 {
		panic("uwm: Geom.Partition called with a non-positive rows or cols value")
	}

	if row < 0 || col < 0 || row >= rows || col >= cols {
		panic("uwm: Geom.Partition called with an out-of-bounds row or col value")
	}

	w := g.W / uint16(cols)
	h := g.H / uint16(rows)
	x := g.X + w*uint16(col)
	y := g.Y + h*uint16(row)

	return Geom{X: x, Y: y, W: w, H: h}
}

// Color is identical to the color.Color interface
type Color interface{ RGBA() (r, g, b, a uint32) }

// Color32 represents a 32-bit ARGB color,
// with bit-format 0xAARRGGBB.
// In typical X11 contexts, such as 24-bit depth windows,
// the alpha bits are ignored.
//
type Color32 uint32

// String returns a hexadecimal representation of the receiver.
func (c Color32) String() string {
	pad := 6
	if c&0xff000000 != 0 {
		pad = 8
	}

	return fmt.Sprintf("#%0*x", pad, uint32(c))
}

// RGBA allows Color32 to implement the image.Color interface.
func (c Color32) RGBA() (r, g, b, a uint32) {
	cc := uint32(c)

	// derived from stdlib image/color package (The Go Authors).
	a = cc >> 24 & 0xff
	a |= a << 8
	r = cc >> 16 & 0xff
	r |= r << 8
	g = cc >> 8 & 0xff
	g |= g << 8
	b = cc & 0xff
	b |= b << 8

	return r, g, b, a
}

func fromColor(c Color) Color32 {
	if c == nil {
		return 0
	}

	c32, ok := c.(Color32)
	if ok {
		return c32
	}

	// derived from stdlib image/color package (The Go Authors).
	r, g, b, a := c.RGBA()
	const m = 0xff00
	return Color32(a&m<<16 | r&m<<8 | g&m | b&m>>8)
}

func minU16(x, y uint16) uint16 {
	if x < y {
		return x
	}

	return y
}

func maxU16(x, y uint16) uint16 {
	if x > y {
		return x
	}

	return y
}
