package uwm

import (
	"image/color"
	"reflect"
	"testing"
)

func TestGeom_IsZero(t *testing.T) {
	tests := []struct {
		name string
		g    Geom
		want bool
	}{
		{
			name: "zero",
			g:    Geom{},
			want: true,
		},
		{
			name: "non_zero",
			g:    Geom{W: 1, H: 1},
			want: false,
		},
	}

	for _, tt := range tests {
		tt := tt // prevent race condition

		t.Run(tt.name, func(t *testing.T) {
			got := tt.g.IsZero()
			if got != tt.want {
				t.Errorf("Geom.IsZero() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGeom_String(t *testing.T) {
	tests := []struct {
		name string
		g    Geom
		want string
	}{
		{
			name: "zero",
			g:    Geom{},
			want: "0x0+0+0",
		},
		{
			name: "positive_offset",
			g:    Geom{X: 1, Y: 2, W: 3, H: 4},
			want: "3x4+1+2",
		},
	}

	for _, tt := range tests {
		tt := tt // prevent race condition

		t.Run(tt.name, func(t *testing.T) {
			got := tt.g.String()
			if got != tt.want {
				t.Errorf("Geom.String() = %q, want %q", got, tt.want)
			}
		})
	}
}

func TestGeom_IsValid(t *testing.T) {
	tests := []struct {
		name string
		g    Geom
		want bool
	}{
		{
			name: "zero",
			g:    Geom{},
			want: false,
		},
		{
			name: "offset_only",
			g:    Geom{X: 1, Y: 2, W: 0, H: 0},
			want: false,
		},
		{
			name: "dimensions_only",
			g:    Geom{X: 0, Y: 0, W: 3, H: 4},
			want: true,
		},
		{
			name: "all_positive",
			g:    Geom{X: 1, Y: 2, W: 3, H: 4},
			want: true,
		},
	}

	for _, tt := range tests {
		tt := tt // prevent race condition

		t.Run(tt.name, func(t *testing.T) {
			got := tt.g.IsValid()
			if got != tt.want {
				t.Errorf("Geom.IsValid() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGeom_NoXY(t *testing.T) {
	tests := []struct {
		name string
		g    Geom
		want Geom
	}{
		{
			name: "zero",
			g:    Geom{},
			want: Geom{},
		},
		{
			name: "non_zero",
			g:    Geom{X: 1, Y: 2, W: 3, H: 4},
			want: Geom{W: 3, H: 4},
		},
	}

	for _, tt := range tests {
		tt := tt // prevent race condition

		t.Run(tt.name, func(t *testing.T) {
			got := tt.g.NoXY()
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Geom.NoXY() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGeom_Overlaps(t *testing.T) {
	tests := []struct {
		name string
		g    Geom
		h    Geom
		want bool
	}{
		{
			name: "diagonal_separation",
			g:    Geom{W: 10, H: 10, X: 0, Y: 0},
			h:    Geom{W: 10, H: 10, X: 10, Y: 10},
			want: false,
		},
		{
			name: "horizontal_separation",
			g:    Geom{W: 10, H: 10, X: 0, Y: 0},
			h:    Geom{W: 10, H: 10, X: 10, Y: 0},
			want: false,
		},
		{
			name: "vertical_separation",
			g:    Geom{W: 10, H: 10, X: 0, Y: 0},
			h:    Geom{W: 10, H: 10, X: 0, Y: 10},
			want: false,
		},
		{
			name: "minimal_overlap",
			g:    Geom{W: 10, H: 10, X: 0, Y: 0},
			h:    Geom{W: 10, H: 10, X: 9, Y: 9},
			want: true,
		},
		{
			name: "maximal_overlap",
			g:    Geom{W: 10, H: 10, X: 0, Y: 0},
			h:    Geom{W: 10, H: 10, X: 0, Y: 0},
			want: true,
		},
		{
			name: "subset",
			g:    Geom{W: 30, H: 30, X: 0, Y: 0},
			h:    Geom{W: 10, H: 10, X: 10, Y: 10},
			want: true,
		},
	}

	for _, tt := range tests {
		tt := tt // prevent race condition

		t.Run(tt.name, func(t *testing.T) {
			got := tt.g.Overlaps(tt.h)
			if got != tt.want {
				t.Errorf("%+v.Overlaps(%+v) = %+v, want %+v", tt.g, tt.h, got, tt.want)
			}

			got = tt.h.Overlaps(tt.g)
			if got != tt.want {
				t.Errorf("%+v.Overlaps(%+v) = %+v, want %+v", tt.h, tt.g, got, tt.want)
			}
		})
	}
}

func TestGeom_Union(t *testing.T) {
	tests := []struct {
		name string
		g    Geom
		h    Geom
		want Geom
	}{
		{
			name: "zeros",
			g:    Geom{},
			h:    Geom{},
			want: Geom{},
		},
		{
			name: "same",
			g:    Geom{W: 20, H: 20, X: 10, Y: 10},
			h:    Geom{W: 20, H: 20, X: 10, Y: 10},
			want: Geom{W: 20, H: 20, X: 10, Y: 10},
		},
		{
			name: "overlap_in_place",
			g:    Geom{W: 10, H: 20, X: 10, Y: 10},
			h:    Geom{W: 20, H: 10, X: 10, Y: 10},
			want: Geom{W: 20, H: 20, X: 10, Y: 10},
		},
		{
			name: "overlap",
			g:    Geom{W: 20, H: 20, X: 10, Y: 10},
			h:    Geom{W: 20, H: 20, X: 20, Y: 20},
			want: Geom{W: 30, H: 30, X: 10, Y: 10},
		},
		{
			name: "disjoint",
			g:    Geom{W: 20, H: 20, X: 10, Y: 10},
			h:    Geom{W: 20, H: 20, X: 20, Y: 20},
			want: Geom{W: 30, H: 30, X: 10, Y: 10},
		},
	}

	for _, tt := range tests {
		tt := tt // prevent race condition

		t.Run(tt.name, func(t *testing.T) {
			got := tt.g.Union(tt.h)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("%+v.Union(%+v) = %+v, want %+v", tt.g, tt.h, got, tt.want)
			}
		})
	}
}

func TestGeom_Partition(t *testing.T) {
	tests := []struct {
		name string
		g    Geom
		rows int
		cols int
		row  int
		col  int
		want Geom
	}{
		{
			name: "3x3_top_left",
			g:    Geom{W: 900, H: 900},
			rows: 3,
			cols: 3,
			row:  0,
			col:  0,
			want: Geom{W: 300, H: 300, X: 0, Y: 0},
		},
		{
			name: "3x3_center",
			g:    Geom{W: 900, H: 900},
			rows: 3,
			cols: 3,
			row:  1,
			col:  1,
			want: Geom{W: 300, H: 300, X: 300, Y: 300},
		},
		{
			name: "3x3_bottom_right",
			g:    Geom{W: 900, H: 900},
			rows: 3,
			cols: 3,
			row:  2,
			col:  2,
			want: Geom{W: 300, H: 300, X: 600, Y: 600},
		},
	}

	for _, tt := range tests {
		tt := tt // prevent race condition

		t.Run(tt.name, func(t *testing.T) {
			got := tt.g.Partition(tt.rows, tt.cols, tt.row, tt.col)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Geom.Partition(%v, %v, %v, %v) = %v, want %v", tt.rows, tt.cols, tt.row, tt.col, got, tt.want)
			}
		})
	}
}

func TestGeom_Partition_panic(t *testing.T) {
	tests := []struct {
		name string
		g    Geom
		rows int
		cols int
		row  int
		col  int
	}{
		{
			name: "zero_rows",
			g:    Geom{W: 900, H: 900},
			rows: 0,
			cols: 1,
			row:  0,
			col:  0,
		},
		{
			name: "zero_cols",
			g:    Geom{W: 900, H: 900},
			rows: 1,
			cols: 0,
			row:  0,
			col:  0,
		},
		{
			name: "negative_rows",
			g:    Geom{W: 900, H: 900},
			rows: -1,
			cols: 1,
			row:  0,
			col:  0,
		},
		{
			name: "negative_cols",
			g:    Geom{W: 900, H: 900},
			rows: 1,
			cols: -1,
			row:  0,
			col:  0,
		},
		{
			name: "negative_row",
			g:    Geom{W: 900, H: 900},
			rows: 1,
			cols: 1,
			row:  -1,
			col:  0,
		},
		{
			name: "negative_col",
			g:    Geom{W: 900, H: 900},
			rows: 1,
			cols: 1,
			row:  0,
			col:  -1,
		},
		{
			name: "bounds_row",
			g:    Geom{W: 900, H: 900},
			rows: 1,
			cols: 1,
			row:  1,
			col:  0,
		},
		{
			name: "bounds_col",
			g:    Geom{W: 900, H: 900},
			rows: 1,
			cols: 1,
			row:  0,
			col:  1,
		},
	}

	for _, tt := range tests {
		tt := tt // prevent race condition

		t.Run(tt.name, func(t *testing.T) {
			var panicked bool

			func() {
				defer func() { panicked = recover() != nil }()
				tt.g.Partition(tt.rows, tt.cols, tt.row, tt.col)
			}()

			if !panicked {
				t.Errorf("Geom.Partition(%v, %v, %v, %v) did not panic",
					tt.rows, tt.cols, tt.row, tt.col)
			}
		})
	}
}

func TestColor32_String(t *testing.T) {
	tests := []struct {
		name string
		c    Color32
		want string
	}{
		{
			name: "zero",
			c:    0x000000,
			want: "#000000",
		},
		{
			name: "red",
			c:    0xff0000,
			want: "#ff0000",
		},
		{
			name: "green",
			c:    0x00ff00,
			want: "#00ff00",
		},
		{
			name: "blue",
			c:    0x0000ff,
			want: "#0000ff",
		},
		{
			name: "alpha",
			c:    0xffffffff,
			want: "#ffffffff",
		},
	}

	for _, tt := range tests {
		tt := tt // prevent race condition

		t.Run(tt.name, func(t *testing.T) {
			got := tt.c.String()
			if got != tt.want {
				t.Errorf("Color32(%x).String() = %q, want %q",
					uint32(tt.c), got, tt.want)
			}
		})
	}
}

func TestColor32_RGBA(t *testing.T) {
	tests := []struct {
		name  string
		c     Color32
		wantR uint32
		wantG uint32
		wantB uint32
		wantA uint32
	}{
		{
			name:  "zero",
			c:     0x000000,
			wantR: 0x0000,
			wantG: 0x0000,
			wantB: 0x0000,
			wantA: 0x0000,
		},
		{
			name:  "red",
			c:     0xff0000,
			wantR: 0xffff,
			wantG: 0x0000,
			wantB: 0x0000,
			wantA: 0x0000,
		},
		{
			name:  "everything",
			c:     0xfedcba98,
			wantR: 0xdcdc,
			wantG: 0xbaba,
			wantB: 0x9898,
			wantA: 0xfefe,
		},
	}

	for _, tt := range tests {
		tt := tt // prevent race condition

		t.Run(tt.name, func(t *testing.T) {
			gotR, gotG, gotB, gotA := tt.c.RGBA()

			if gotR != tt.wantR {
				t.Errorf("Color32.RGBA() = %x [gotR], want %x", gotR, tt.wantR)
			}

			if gotG != tt.wantG {
				t.Errorf("Color32.RGBA() = %x [gotG], want %x", gotG, tt.wantG)
			}

			if gotB != tt.wantB {
				t.Errorf("Color32.RGBA() = %x [gotB], want %x", gotB, tt.wantB)
			}

			if gotA != tt.wantA {
				t.Errorf("Color32.RGBA() = %x [gotA], want %x", gotA, tt.wantA)
			}
		})
	}
}

func Test_fromColor(t *testing.T) {
	tests := []struct {
		name string
		c    Color
		want Color32
	}{
		{
			name: "nil",
			c:    nil,
			want: 0x00000000,
		},
		{
			name: "black",
			c:    color.Gray{Y: 0},
			want: 0xff000000,
		},
		{
			name: "color32",
			c:    Color32(0xff0000),
			want: 0xff0000,
		},
	}

	for _, tt := range tests {
		tt := tt // prevent race condition

		t.Run(tt.name, func(t *testing.T) {
			got := fromColor(tt.c)
			if got != tt.want {
				t.Errorf("fromColor(%v) = %v , want %v", tt.c, got, tt.want)
			}
		})
	}
}
